package com.example.m7m8_map.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.m7m8_map.R
import com.example.m7m8_map.databinding.MarkerListItemBinding
import com.example.m7m8_map.model.Marker
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.m7m8_map.view.RecyclerFragment
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class MarkerAdapter(private var markers: List<Marker>, private val listener: MyOnClickListener):
    RecyclerView.Adapter<MarkerAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = MarkerListItemBinding.bind(view)
        fun setListener(marker: Marker){
            binding.itemListDetail.setOnClickListener {
                listener.detail(marker)
            }
            binding.itemListMap.setOnClickListener{
                listener.map(marker)

            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.marker_list_item, parent, false)
        return ViewHolder(view)

    }

    @SuppressLint("NotifyDataSetChanged")
    fun setMarkerList(markerList: MutableList<Marker>){
        this.markers = markerList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marker = markers[position]

        with(holder){
            setListener(marker)
            binding.itemListText.text = marker.name
            val storage = FirebaseStorage.getInstance().reference.child("images/marker.image")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.itemListImage.setImageBitmap(bitmap)

            }
//            Glide.with(context)
//                .load(marker.image)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .centerCrop()
//                .circleCrop()
//                .into(binding.itemListImage)
        }
    }

    override fun getItemCount(): Int {
        return markers.size
    }
}