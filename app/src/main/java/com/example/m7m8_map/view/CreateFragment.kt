package com.example.m7m8_map.view

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

import com.example.m7m8_map.R
import com.example.m7m8_map.databinding.FragmentCreateBinding
import com.example.m7m8_map.model.Marker
import com.example.m7m8_map.viewModel.MarkerViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*

class CreateFragment : Fragment() {
    lateinit var imageUri : Uri
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                imageUri = data.data!!
                binding.inputPicture.setImageURI(imageUri)
            }
        }
    }

    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    lateinit var binding: FragmentCreateBinding
     private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCreateBinding.inflate(layoutInflater)
        binding.inputPicture.setImageResource(R.drawable.blank_image)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.inputName.setText(viewModel.name)
        binding.camera.setOnClickListener{
            viewModel.name = binding.inputName.text.toString()
            findNavController().navigate(R.id.action_createFragment_to_cameraFragment2)
        }
        binding.gallery.setOnClickListener{
            selectImage()

        }
        binding.inputPicture.setImageURI(viewModel.image)


        binding.createMarker.setOnClickListener {


            imageUri = viewModel.image!!
            val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
            val now = Date()
            val fileName = formatter.format(now)
            val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
            storage.putFile(imageUri)
                .addOnSuccessListener {
                    binding.inputPicture.setImageURI(null)
                    db.collection("users").document(auth.currentUser?.email!!).collection("markers").document(binding.inputName.text.toString()).set(
                        hashMapOf("latitude" to viewModel.latitude,
                            "longitude" to viewModel.longitude,
                            "name" to binding.inputName.text.toString(),
                            "image" to fileName,
                            "desc" to binding.inputDesctiption.text.toString())
                    )
                    Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                }

//            var latitude = viewModel.latitude
//            var longitude = viewModel.longitude
//            var name = binding.inputName.text.toString()
//            var image = viewModel.image
//            var desc = binding.inputDesctiption.text.toString()
//            viewModel.addMarker(Marker(latitude, longitude, name, image!!.toString(), desc))
            findNavController().navigate(R.id.action_createFragment_to_mapFragment)
        }
    }
    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }

}




